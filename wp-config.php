<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'dribbble1db');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Ctfrs>QN3YMfV[f+3Qq8>/v R/w8<6KD)`:]a0vNdXa@m|#0L`kXaD-Dj(%U41uF');
define('SECURE_AUTH_KEY',  'G(gT67c.r{0Yi8WX>=;D{^9Iq3pB$DD#|ZFs>?Yc(QxBbDm7H-ZlJLK%<jropo/|');
define('LOGGED_IN_KEY',    ';Im|McBh35FtD+:^o5Nuxc6`)%tNRM*_QGYch 6,lA4#YV1x~oi6BC0H+:O0 wqq');
define('NONCE_KEY',        '],o_Mk!|/xOv}>H6|pI`0B4uRX[~jFm[?TIVO)C4/|21eeqBD9yo&kAAXp6!]GD*');
define('AUTH_SALT',        '7i[H-Z!)+9d:2>.cyZ*D@qp<lP{7#/d>%b?Go$k,v-^)-Ue/30nI/1!r7^%Zznce');
define('SECURE_AUTH_SALT', '~#_XHETa]{qLA)oPG-w:k>ds*D`q$MH&O*T*f6KsB5vb/sZtZ9=XGk<-[F!!&kqk');
define('LOGGED_IN_SALT',   '(a--5~(g/%iO}]K^4@t$_>M ^YbvA1h8-4%Ltl2Vm<DGB`z84]*t3]O~Ov::jtNs');
define('NONCE_SALT',       'O`(h,J|Gn- LO#~b4]a,EL`ZQq!er9`[4sw@%#8uN|M-rsbC6W;(ib=L.Q]o}( Y');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
